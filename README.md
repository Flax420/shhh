# SHHH
A program to stop loud children from screaming so much. This is intended for use on Windows

## Installation
You will need Python3, pip3, numpy, sounddevice.  
First you will need to install Python which you can get from [here](https://www.python.org/downloads/).  
You should install Python to `C:\Python37` to make this work properly.  
Then you should install numpy and sounddevice. To do this simply type `pip install numpy sounddevice`.  
Next you will need to install autostart.bat, to do this open Run with `Win+R` and type in `shell:startup` and put autostart.bat in that folder.  
Finally you need to start Run again and type in %appdata% and create a new folder and call it "shhh" and put main.py in there.  
Now simply reboot and open the task manager and look for `Python.exe` 

## Configuration
Simply edit main.py with a text editor and change the variables at the top to change how the program behaves.


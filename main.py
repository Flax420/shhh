import sounddevice as sd
import numpy as np
import time
from subprocess import call

timeout = 2 # seconds
volume_to_kill = 70 # Print volume_norm to find a good value and change it to that
samples_per_second = 38 # This is probably the same on all machines
counter = 0 # This does not need to be touched

def print_sound(indata, outdata, frames, time, status):
    global counter
    volume_norm = np.linalg.norm(indata)*10
    if(volume_norm > volume_to_kill):
        if(counter > timeout*samples_per_second):
            # Kill computer
            call(["shutdown", "-s", "-t", "0"])
        else:
            counter += 1
    elif(counter > 0):
        counter -= 0.5

with sd.Stream(callback=print_sound):
    while True:
        time.sleep(10000) # Keeps down CPU usage
